<?php

require 'referral/admin/functions.php';
require 'referral/ajax.php';
require 'referral/functions.php';

add_action( 'wp_enqueue_scripts', 'parent_theme_enqueue_styles' );
function parent_theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

	/*
	 * Loads the main stylesheet.
	 */
	wp_enqueue_style( 'divi-child-style', get_stylesheet_uri(), array() );

	?>
	<script>
        var ajaxUrl = '<?php echo admin_url('admin-ajax.php'); ?>';
	</script>
	<?php
}