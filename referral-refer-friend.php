<?php
/**
 * Template Name: Refer Friend
 */

add_action( 'wp_enqueue_scripts', function() {
	// jQuery validate
	wp_enqueue_script( 'referral-jquery-validate', get_stylesheet_directory_uri() . '/assets/js/jquery.validate.min.js', array( 'jquery' ));

	// Load script for front page template
	wp_enqueue_script( 'referral-notification', REF__HOME_DIR . '/assets/js/referral.notification.js', array( 'jquery' ));

	// Load script for front page template
	wp_enqueue_script( 'referral-scripts', get_stylesheet_directory_uri() . '/assets/js/referral.js', array( 'jquery' ));
});

$referral_user = referral__get_user();

$is_invitations_allowed = TRUE;
if( $referral_user->invitations_used >= $referral_user->invitations_total ) {
    $is_invitations_allowed = FALSE;
}

get_header(); ?>

	<div class="referral">
    <?php if( !$referral_user->blocked ) : ?>
		<div class="container clearfix">
			<?php referral__get_user_sidebar(); ?>
			<div class="referral__inner referral__refer-friend">
				<div class="referral__content clearfix">
					<div class="referral__left clearfix">
                        <h2 class="referral__title">Give Your Friends a FREE Trial & $500 Off</h2>
                        <div class="referral__text">
                            <?php
                            while ( have_posts() ) { the_post();
                                the_content();
                            }
                            ?>
                        </div>
					</div>
					<div class="referral__right">

                        <iframe src="https://player.vimeo.com/video/309144510" width="238" height="133" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                        <h4 class="referral__video-title">Give your friends a free trial and $500 off.</h4>
                        <p class="referral__video-text">Refer-a-friend used to be capped to 3 invites only but now is UNLIMITED! Refer as many people as you want! Some students are making $500-$2,000/day just by referring friends... If you want to make some seriously easy money, this is for you!</p>
					</div>
				</div>
                <div class="referral__content referral__invite">
                    <h2 class="referral__title">Invite A friend</h2>
                    <?php
                    $invite_class = '';
                    if( !$is_invitations_allowed ) {
                        $invite_class = 'referral__invite-disable';
                        ?>
                        <h3 class="referral__disable-title">You have reached the limit of friends invitation.</h3>
                        <?php
                    }
                    ?>
                    <div class="<?php echo $invite_class; ?>">
                        <h5 class="referral__sub-title">HEADS UP: Having a profile picture improves the chances of your friends signing up because we can personalize the experience. Click here to upload one.</h5>
                        <form class="referral__invite-form">
                            <div class="clearfix">
                                <div class="f-name">
                                    <h4>First Name</h4>
                                    <input type="text" name="friend-name" placeholder="Friend's first name">
                                </div>
                                <div class="e-address">
                                    <h4>Email Address</h4>
                                    <input type="email" name="friend-email" placeholder="Friend's email address">
                                </div>
                            </div>
                            <div class="message">
                                <h4>Message</h4>
                                <div class="message__inner">
                                    <p class="message-gray name" style="color: rgba(102, 102, 102, 0.5);">Hey <span data-default="[Name]">[Name]</span></p>
                                    <div contenteditable="true">
	                                    <?php echo referral__settings_get('email-body-text'); ?>
                                    </div>
                                    <p class="message-gray info" style="color: rgba(102, 102, 102, 0.5);">Click the button below to accept my invitation and<br>learn more.</p>
                                </div>
                            </div>
                            <input type="submit" value="Refer Friend #<?php echo (int)($referral_user->invitations_used)+1; ?>">
                        </form>
                    </div>
                </div>
            </div>
		</div>
    <?php else: ?>
        <h1 class="blocked-error">Your referral account was blocked!<br>Please write to Administrator for more information.</h1>
    <?php endif; ?>
	</div>

<?php get_footer();