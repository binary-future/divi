<?php
/**
 * Template Name: Referral
 */

add_action( 'wp_enqueue_scripts', function() {
	// jQuery validate
	wp_enqueue_script( 'referral-jquery-validate', REF__HOME_DIR . '/assets/js/jquery.validate.min.js', array( 'jquery' ));

	// Load script for front page template
	wp_enqueue_script( 'referral-notification', REF__HOME_DIR . '/assets/js/referral.notification.js', array( 'jquery' ));

	// Load script for front page template
	wp_enqueue_script( 'referral-scripts', REF__HOME_DIR . '/assets/js/referral.js', array( 'jquery' ));
});

get_header();

$referral_user = referral__get_user();
?>

<div class="referral">
    <?php if( !$referral_user->blocked ) : ?>
	<div class="container clearfix">
		<?php referral__get_user_sidebar(); ?>
		<div class="referral__inner">
            <div class="referral__content">
                <div class="referral__withdraw clearfix">
                    <div class="clearfix">
                        <h2 class="referral__title">Tracking & Earnings</h2>
                        <?php
                        $withdraw_class = '';

                        if( referral__is_user_withdraw_request() ){
	                        $withdraw_class = 'disable';
                        } ?>
                        <a href="#" class="referral__withdraw-btn <?php echo $withdraw_class; ?>">Withdraw Balance</a>
                    </div>
                    <div class="referral__stats clearfix">
                        <div class="referral__stat referrals">
                            <span class="referral__icon">
                                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/your-referrals.png'; ?>">
                            </span>
                            <span class="referral__text">Your<br>Referrals</span>
                            <span class="referral__value" data-val="<?php echo referral__get_user_leads_count(); ?>">0</span>
                        </div>
                        <div class="referral__stat earning">
                            <span class="referral__icon">
                                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/your-earnings.png'; ?>">
                            </span>
                            <span class="referral__text">Your<br>Earning</span>
                            <span class="referral__value">$ <span data-val="<?php echo $referral_user->earning; ?>">0</span></span>
                        </div>
                        <div class="referral__stat balance">
                            <span class="referral__icon">
                                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/your-balance.png'; ?>">
                            </span>
                            <span class="referral__text">Your<br>Balance</span>
                            <?php
                            $pending_balance = 0;
                            if( $referral_user->pending ) {
                                $pending_balance = $referral_user->pending;
                            }
                            ?>
                            <span class="referral__value">$ <span data-val="<?php echo $referral_user->balance; ?>">0</span><span class="referral__value-small">($ <?php echo $pending_balance; ?> Pending)</span></span>
                        </div>
                    </div>
                    <p class="referral__withdraw-text">Above you can see your successful referrals, earnings and account balance. Any funds showing as “pending” in you account balance are being held for 15 days to make sure the referred customer doesn’t refund or dispute the transaction before paying out your commission. After funds have been held for 15 days they will become available in your account balance and you can withdraw them.</p>
                </div>
            </div>
            <div class="referral__content invitations">
                <div class="referral__withdraw clearfix">
                    <div class="clearfix">
                        <h2 class="referral__title">Invitations - <?php echo $referral_user->invitations_total; ?></h2>
                    </div>
                    <div class="invitations__items">
                        <div class="invitations__item">
                            <div class="invitations__graph">
                                <svg viewBox="0 0 36 36" width="215px" height="215px" class="circular-chart">
                                    <defs>
                                        <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
                                            <stop offset="0%" stop-color="#FAD961" />
                                            <stop offset="100%" stop-color="#F66B1C" />
                                        </linearGradient>
                                    </defs>
                                    <path class="circle"
                                          stroke="url(#gradient)"
                                          stroke-dasharray="<?php echo round(100/$referral_user->invitations_total*$referral_user->invitations_used); ?>, 100"
                                          d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"/>
                                </svg>
                                <span class="invitations__item-percents"><span data-val="<?php echo round(100/$referral_user->invitations_total*$referral_user->invitations_used); ?>">0</span><span>%</span></span>
                            </div>
                            <p class="invitations__item-text">Invitations Used - <span><?php echo $referral_user->invitations_used; ?></span></p>
                        </div>
	                    <?php
                        $invitations_success = referral__get_user_success_leads_count();
                        $invitations_total_sent = referral__get_referral_total_invitation_sent();
                        ?>
                        <div class="invitations__item">
                            <div class="invitations__graph">
                                <svg viewBox="0 0 36 36" width="215px" height="215px" class="circular-chart">
                                    <defs>
                                        <linearGradient id="gradient1" x1="0%" y1="0%" x2="0%" y2="100%">
                                            <stop offset="0%" stop-color="#F14F5E" />
                                            <stop offset="100%" stop-color="#9F041C" />
                                        </linearGradient>
                                    </defs>
                                    <path class="circle"
                                          stroke="url(#gradient1)"
                                          stroke-dasharray="<?php echo round(100/$invitations_total_sent*$invitations_success); ?>, 100"
                                          d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"/>
                                </svg>
                                <span class="invitations__item-percents"><span data-val="<?php echo round(100/$invitations_total_sent*$invitations_success); ?>">0</span><span>%</span></span>
                            </div>
                            <p class="invitations__item-text">Invitations Success - <span><?php echo $invitations_success; ?></span></p>
                        </div>
	                    <?php $invitations_left = $referral_user->invitations_total - $referral_user->invitations_used; ?>
                        <div class="invitations__item">
                            <div class="invitations__graph">
                                <svg viewBox="0 0 36 36" width="215px" height="215px" class="circular-chart">
                                    <defs>
                                        <linearGradient id="gradient2" x1="0%" y1="0%" x2="0%" y2="100%">
                                            <stop offset="0%" stop-color="#C86DD7" />
                                            <stop offset="100%" stop-color="#3223AE" />
                                        </linearGradient>
                                    </defs>
                                    <path class="circle"
                                          stroke="url(#gradient2)"
                                          stroke-dasharray="<?php echo round(100/$referral_user->invitations_total*$invitations_left); ?>, 100"
                                          d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"/>
                                </svg>
                                <span class="invitations__item-percents"><span data-val="<?php echo round(100/$referral_user->invitations_total*$invitations_left); ?>">0</span><span>%</span></span>
                            </div>
                            <p class="invitations__item-text">Invitations Left - <span><?php echo $invitations_left; ?></span></p>
                        </div>
                    </div>
                    <div class="links-used">
                        <table>
                            <thead>
                                <tr>
                                    <td class="name">Name</td>
                                    <td class="email">Email</td>
                                    <td class="referred">Referred</td>
                                    <td class="expires">Expires</td>
                                    <td class="status">Status</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ( referral__get_user_leads() as $lead ) {
                                $expired_date = $lead->referred_date + ( 60 * 60 * 24 * REFERRAL__TO_PENDING_DAYS );
                                $status = referral__get_status_text($lead->status);
                                ?>
                                <tr>
                                    <td class="name"><?php echo $lead->name; ?></td>
                                    <td class="email"><?php echo $lead->email ?></td>
                                    <td class="referred"><?php echo date('m/d/Y', $lead->referred_date); ?></td>
                                    <td class="expires"><?php echo date('m/d/Y', $expired_date); ?></td>
                                    <td class="status <?php echo $status; ?>"><?php echo $status; ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <p class="referral__withdraw-text">Above you can see your successful referrals, earnings and account balance. Any funds showing as “pending” in you account balance are being held for 15 days to make sure the referred customer doesn’t refund or dispute the transaction before paying out your commission. After funds have been held for 15 days they will become available in your account balance and you can withdraw them.</p>
                </div>
            </div>
		</div>
	</div>
    <div class="popup">
        <div class="popup__inner">
            <form class="withdraw-form">
                <div class="popup__container">
                    <h3 class="popup__title">Withdraw</h3>
                    <div class="referral__stats clearfix">
                        <div class="referral__stat referrals">
                                <span class="referral__icon">
                                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/your-referrals.png'; ?>">
                                </span>
                            <span class="referral__text">Your<br>Referrals</span>
                            <span class="referral__value" data-val="<?php echo referral__get_user_leads_count(); ?>">0</span>
                        </div>
                        <div class="referral__stat earning">
                                <span class="referral__icon">
                                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/your-earnings.png'; ?>">
                                </span>
                            <span class="referral__text">Your<br>Earning</span>
                            <span class="referral__value">$ <span data-val="<?php echo $referral_user->earning; ?>">0</span></span>
                        </div>
                        <div class="referral__stat balance">
                                <span class="referral__icon">
                                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/your-balance.png'; ?>">
                                </span>
                            <span class="referral__text">Your<br>Balance</span>
                            <span class="referral__value">$ <span data-val="<?php echo $referral_user->balance; ?>">0</span><span class="referral__value-small">($ <?php echo $referral_user->pending; ?> Pending)</span></span>
                        </div>
                    </div>
                    <div class="popup__payment">
                        <h3 class="popup__title">Payment System</h3>
                        <div class="popup__payment-block clearfix">
                            <div class="left">
                                <h5>Amount</h5>
                                <div>
                                    <span class="popup__payment-dollar">$</span>
                                    <input type="text" placeholder="0" name="payment-amount">
                                </div>
                            </div>
                            <div class="right">
                                <h5>Available Balance - </h5>
                                <span>$ </span>
                                <span><?php echo $referral_user->balance; ?></span>
                            </div>
                        </div>
                        <div class="popup__pay-methods clearfix">
                            <div class="popup__pay-method paypal active">
                                <div class="popup__pay-image">
                                    <img src="<?php echo REF__HOME_DIR . '/assets/images/paypal.png' ?>" alt="paypal">
                                    <div class="popup__pay-radio">
                                        <input type="radio" name="pay-method" value="paypal" checked>
                                    </div>
                                </div>
                                <h4 class="popup__pay-title">Enter in field your PayPal Email</h4>
                                <input type="email" name="paypal-email" placeholder="infol@info.com">
                            </div>
                            <div class="popup__pay-method bank-transfer">
                                <div class="popup__pay-image">
                                    <img src="<?php echo REF__HOME_DIR . '/assets/images/bank-transfer.png' ?>" alt="bank-transfer">
                                    <div class="popup__pay-radio">
                                        <input type="radio" name="pay-method" value="bank-transfer">
                                    </div>
                                </div>
                                <h4 class="popup__pay-title">Enter in field your Bank account</h4>
                                <textarea disabled name="bank-transfer-data" placeholder="If you are sending a wire transfer, you will need to  provide the following information about  the recipient: ABA/Routing number of the receiving bank. Recipient’s bank account number."></textarea>
                            </div>
                        </div>
                        <div class="popup__pay-password">
                            <h4>Password</h4>
                            <input type="password" placeholder="*********************" name="pay-password">
                        </div>
                        <div class="popup__buttons">
                            <a href="#" class="popup__button-submit">Withdraw Balance</a>
                            <a href="#" class="popup__button-cancel">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php else: ?>
    <h1 class="blocked-error">Your referral account was blocked!<br>Please write to Administrator for more information.</h1>
    <?php endif; ?>
</div>

<?php get_footer();