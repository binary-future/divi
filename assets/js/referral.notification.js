jQuery(document).ready(function ($) {
    $('body').append('<p class="referral__message"></p>');
    var notificationBlock = $('.referral__message');

    jQuery.referralNotification = {
        /**
         * Show notice
         *
         * @param text
         * @param status
         * @param flashNotice - set tot true if need to close it after timeOut
         */
        show: function (text, status, flashNotice) {
            if( flashNotice === undefined ) {
                flashNotice = false;
            }

            if( status === undefined ) {
                status = '';
            }

            notificationBlock
                .removeClass('error')
                .addClass(status)
                .text(text)
                .css({'left': 'calc(50% - ' + notificationBlock.width() / 2 + 'px)', 'bottom': '20px'});

            if( flashNotice ) {
                setTimeout(function () {
                    jQuery.referralNotification.hide();
                }, 5000);
            }
        },
        hide: function () {
            notificationBlock.css('bottom', '-50px');
            setTimeout(function () {
                notificationBlock.removeAttr('style');
            }, 500);

        }
    };
});
