jQuery(document).ready(function ($) {
    // animate counts
    $('[data-val]').each(function () {
        animateValue($(this), $(this).attr('data-val'))
    });

    function animateValue($item, range) {
        var current = 0;
        // var increment = 1;
        var increment = Math.round(range/100);
        if( increment < 1 ) {
            increment = 1;
        }
        var stepTime = Math.abs(Math.floor(1000 / range));

        var timer = setInterval(function() {
            current += increment;
            if (current == range) {
                clearInterval(timer);
            }
            else if( current > range ) {
                current = range;
                clearInterval(timer);
            }
            $item.text( current );
        }, stepTime);
    }

    $('body').on('input change', '.referral__invite-form input[type="text"]', function () {
        var currentValue = $(this).val();
        var targetBlock = $('.referral__invite-form .name span')
        if( currentValue === '' ) {
            targetBlock.text(targetBlock.attr('data-default'));
        }
        else {
            targetBlock.text(currentValue);
        }
    });

    var ajaxSending = false;
    // Validate add lead user form
    $('.referral__invite-form').validate({
        rules: {
            'friend-name': 'required',
            'friend-email': {
                required: true,
                email: true
            }
        },
        submitHandler: function (form) {
            if( !ajaxSending ) {
                jQuery.referralNotification.show('Sending...');

                var settingsData = {
                    'action': 'referral_ajax__add_lead',
                    'dataType': 'JSON',
                    'data': {
                        'name': $('[name="friend-name"]').val(),
                        'email': $('[name="friend-email"]').val(),
                        'message': $('.message__inner').html(),
                    }
                };

                jQuery.post(ajaxUrl, settingsData, function (response) {
                    var messageClass = '';
                    if (!response.status) {
                        messageClass = 'error';
                    }
                    jQuery.referralNotification.show(response.msg, messageClass, true);

                    // if lead added successful, reload page
                    if( response.status ) {
                        document.location.reload();
                    }
                });
            }
        }
    });

    $('.popup__pay-radio input').change(function (e) {
        e.preventDefault();

        $('.popup__pay-method.active').removeClass('active');
        var parentBlock = $(this).parents('.popup__pay-method');
        parentBlock.addClass('active');

        if( parentBlock.hasClass( 'paypal' ) ) {
            $('textarea[name="bank-transfer-data"]').prop('disabled', 'disabled');
            $('input[name="paypal-email"]').prop('disabled', '');
        }
        else if( parentBlock.hasClass( 'bank-transfer' ) ) {
            $('input[name="paypal-email"]').prop('disabled', 'disabled');
            $('textarea[name="bank-transfer-data"]').prop('disabled', '');
        }
    });

    $('.popup__buttons a.popup__button-cancel, .popup').click(function (e) {
        if( this === e.target ) {
            e.preventDefault();
            $('.popup').fadeOut();
        }
    });

    $('.referral__withdraw-btn').click(function (e) {
        e.preventDefault();
        if( !$(this).hasClass('disable') ) {
            $('.popup').fadeIn();
        }
    });

    $('.popup__button-submit').click(function (e) {
        e.preventDefault();

        $('.withdraw-form').submit();
    });

    $('.withdraw-form').validate({
        rules: {
            'payment-amount': 'required',
            'pay-password': 'required',
            'bank-transfer-data': 'required',
            'paypal-email': {
                required: true,
                email: true
            }
        },
        submitHandler: function (form) {
            // check is withdraw amount not more then current balance
            var amount = $('[name="payment-amount"]').val();

            if( parseInt( amount ) > parseInt( $('.popup__payment-block .right span:last-of-type').text() ) ) {
                jQuery.referralNotification.show('Amount can not be more then balance.', 'error', true);
            }
            else {
                jQuery.referralNotification.show('Sending...');

                var data = {
                    'password': $('[name="pay-password"]').val(),
                    'type': $('.popup__pay-radio input:checked').val(),
                    'amount': amount
                };

                if( data.type === 'paypal' ) {
                    data.data = $('input[name="paypal-email"]').val();
                }
                else {
                    data.data = $('textarea[name="bank-transfer-data"]').val();
                }

                var settingsData = {
                    'action': 'referral_ajax__add_withdraw_request',
                    'dataType': 'JSON',
                    'data': data
                };

                let isAjaxBusy = false;
                if( !isAjaxBusy ) {
                    isAjaxBusy = true;
                    jQuery.post(ajaxUrl, settingsData, function (response) {
                        // some shit goes here
                        isAjaxBusy = false;
                    });
                }
            }
        }
    });
});