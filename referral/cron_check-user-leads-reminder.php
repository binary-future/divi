<?php

/**
 * Check check is need send reminder to user via cron, calling 1 time per day
 */

require_once( dirname( dirname( dirname( dirname( dirname( __FILE__ ))))) . '/wp-load.php' );

global $wpdb;

# get leads with status SENT
$leads = $wpdb->get_results( "SELECT * FROM " . REFERRAL__TABLE_LEADS . " WHERE status = '" . REFERRAL__LEAD_SENT . "'" );

foreach ( $leads as $lead ) {
    $days_from_referred_date = round((time() - $lead->referred_date)/86400);
    $send_email = FALSE;

    if( $days_from_referred_date == 1 ) {
        $send_email = TRUE;
    }
    else if( $days_from_referred_date == 5 ) {
        $send_email = TRUE;

    }
    else if( $days_from_referred_date == 10 ) {
        $send_email = TRUE;

    }

    if( $send_email ) {
        $user = get_user_by( 'ID', $lead->user_id );

        // Email to lead
        $user_subject = referral__settings_get('reminder-user');
        $user_body    = referral__settings_get('user-email-body-text');
        $user_to      = $lead->email;
        $invite_url   = site_url() . "/referral-invitation/?c={$lead->invite_hash}";
        $user_body   .= '<p>Link will be expired on: <strong>' . date('Y-m-d', $lead->referred_date + (86400*REFERRAL__TO_PENDING_DAYS)) . '</strong></p>';
        $user_body   .= '<p><a href="' . $invite_url . '" style="padding: 13px 25px; background-color: #2389e1; color: #ffffff; text-align: center; font-size: 16px; margin: 15px 0 0; display: inline-block; text-decoration: none;">Join Drop Ship Lifestyle</a></p>';
        $headers   = array( 'Content-Type: text/html; charset=UTF-8;' );
        $headers[] = 'From: ' . $user->display_name . '<referral@dropshiplifestyle.com>';

        wp_mail( $user_to, $user_subject, $user_body, $headers );

        // Email to user
        $person_subject = referral__settings_get('reminder-person');
        $person_body    = referral__settings_get('person-email-body-text');
        $person_to      = $user->user_email;
        $person_body   .= '<p>The link for <strong>' . $lead->name . '</strong> will be expired on data: <strong>' . date('Y-m-d', $lead->referred_date + (86400*REFERRAL__TO_PENDING_DAYS)) . '</strong></p>';
        $person_body   .= '<p><a href="' . $invite_url . '" style="padding: 13px 25px; background-color: #2389e1; color: #ffffff; text-align: center; font-size: 16px; margin: 15px 0 0; display: inline-block; text-decoration: none;">Join Drop Ship Lifestyle</a></p>';
        $person_headers   = array( 'Content-Type: text/html; charset=UTF-8;' );
        $person_headers[] = 'From: Drop Ship Lifestyle <referral@dropshiplifestyle.com>';

        wp_mail( $person_to, $person_subject, $person_body, $person_headers );
    }
}

