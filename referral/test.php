<?php

require_once( dirname( dirname( dirname( dirname( dirname( __FILE__ ))))) . '/wp-load.php' );

global $wpdb;

$month1 = date('Y-m-01');
$month1_name = date('M', strtotime($month1));

$month2 = date('Y-m-d', strtotime(date('Y-m-01').' -1 MONTH'));
$month2_name = date('M', strtotime($month2));

$month3 = date('Y-m-d', strtotime(date('Y-m-01').' -2 MONTH'));
$month3_name = date('M', strtotime($month3));

$month4 = date('Y-m-d', strtotime(date('Y-m-01').' -3 MONTH'));
$month4_name = date('M', strtotime($month4));

$month5 = date('Y-m-d', strtotime(date('Y-m-01').' -4 MONTH'));
$month5_name = date('M', strtotime($month5));

$month6 = date('Y-m-d', strtotime(date('Y-m-01').' -5 MONTH'));

$month1_count =
$month1_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month1}' AND user_registered > '{$month2}'" );
$month2_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month2}' AND user_registered > '{$month3}'" );
$month3_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month3}' AND user_registered > '{$month4}'" );
$month4_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month4}' AND user_registered > '{$month5}'" );
$month5_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month5}' AND user_registered > '{$month6}'" );

//echo $month1_name . ' : ' . $month1_count . "<br>";
//echo $month2_name . ' : ' . $month2_count . "<br>";
//echo $month3_name . ' : ' . $month3_count . "<br>";
//echo $month4_name . ' : ' . $month4_count . "<br>";
//echo $month5_name . ' : ' . $month5_count . "<br>";

$withdraw_date = strtotime( date('Y-m-d').' -3 MONTH');
//echo $withdraw_date;
$withdraw_requests = $wpdb->get_results( "SELECT * FROM " . REFERRAL__TABLE_WITHDRAW . " WHERE date > '{$withdraw_date}'" );
//print_r( $withdraw_requests );

//echo strtotime($month6);
$leads_last_date = strtotime($month6);
$leads_data = $wpdb->get_results( "SELECT * FROM " . REFERRAL__TABLE_LEADS . " WHERE referred_date > '{$leads_last_date}'" );

//print_r( $leads_data );
# chart 3
$leads = array(
    $month1 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 ),
    $month2 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 ),
    $month3 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 ),
    $month4 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 ),
    $month5 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 )
);

foreach ( $leads_data as $lead ) {
    # get lead month
    $month = date('Y-m-01', $lead->referred_date);

    $leads[$month][$lead->status]++;
}

echo "<pre>";
print_r( $leads );
echo "</pre>";