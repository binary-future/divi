<?php

/**
 * Add lead and send referral email
 */
add_action('wp_ajax_referral_ajax__add_lead', 'referral_ajax__add_lead');
add_action('wp_ajax_nopriv_referral_ajax__add_lead', 'referral_ajax__add_lead');

function referral_ajax__add_lead() {
	$result = array(
		'status' => FALSE,
		'msg' => 'Please login'
	);
	if ( is_user_logged_in() ) {
		if ( isset( $_POST['data'] ) ) {

			if(!referral__is_user_exist($_POST['data']['email'])) {
				if ( ! referral__is_lead_exist( $_POST['data']['email'] ) ) {
					$referral_user = referral__get_user();

					if( $referral_user->invitations_used < $referral_user->invitations_total ) {

						$message = str_replace( "\'", "&apos;", $_POST['data']['message'] );

                        $invite_hash = md5(uniqid(rand(), true));

						referral__add_lead( $_POST['data']['email'], $_POST['data']['name'], $referral_user, $message, $invite_hash );

						$invite_url = site_url() . "/referral-invitation/?c={$invite_hash}";

						$to        = $_POST['data']['email'];
						$subject   = referral__settings_get( 'email-subject' );
						$body      = $message;
						$body      .= '<p><a href="' . $invite_url . '" style="padding: 13px 25px; background-color: #2389e1; color: #ffffff; text-align: center; font-size: 16px; margin: 15px 0 0; display: inline-block; text-decoration: none;">Join Drop Ship Lifestyle</a></p>';
						$headers   = array( 'Content-Type: text/html; charset=UTF-8;' );
						$headers[] = 'From: ' . $referral_user->display_name . '<referral@ssl.wdscode.guru>';
						wp_mail( $to, $subject, $body, $headers );

						$result['status'] = TRUE;
						$result['msg'] = 'Referral email send!';
					}
					else {
						$result['msg'] = 'You have reached the limit of friends invitation.';
					}
				} else {
					$result['msg'] = 'Referral link to this user was already sent.';
				}
			}
			else {
				$result['msg'] = 'User with this email already created.';
			}
		}
		else {
			$result['msg'] = 'Wrong Params.';
		}
	}
	wp_send_json($result);
	die();
}

/**
 * Update referral settings
 */
add_action('wp_ajax_referral_ajax__save_settings', 'referral_ajax__save_settings');
add_action('wp_ajax_nopriv_referral_ajax__save_settings', 'referral_ajax__save_settings');

function referral_ajax__save_settings() {
	$message = 'Error!';
	if( is_user_logged_in() && current_user_can('administrator') ) {
		if ( isset( $_POST['data'] ) ) {

			referral__settings_set('commission',             $_POST['data']['commission']);
			referral__settings_set('referrals',              $_POST['data']['referrals']);
			referral__settings_set('email-subject',          $_POST['data']['email-subject']);
			referral__settings_set('email-body-text',        $_POST['data']['email-body-text']);
			referral__settings_set('reminder-person',        $_POST['data']['reminder-person']);
			referral__settings_set('person-email-body-text', $_POST['data']['person-email-body-text']);
			referral__settings_set('reminder-user',          $_POST['data']['reminder-user']);
			referral__settings_set('user-email-body-text',   $_POST['data']['user-email-body-text']);

			$message = 'Saved!';
		}
		else {
			$message = 'Wrong Data!';
		}
	}
	die( $message );
}

/**
 * Add referral withdraw request
 */
add_action('wp_ajax_referral_ajax__add_withdraw_request', 'referral_ajax__add_withdraw_request');
add_action('wp_ajax_nopriv_referral_ajax__add_withdraw_request', 'referral_ajax__add_withdraw_request');

function referral_ajax__add_withdraw_request () {
	$result = array(
		'status' => FALSE,
		'msg' => 'Error'
	);

	if( is_user_logged_in() ) {
		if( isset( $_POST['data'] ) ) {
			$current_user = wp_get_current_user();

			# check is user do not have active withdraw request
			if( !referral__is_user_withdraw_request() ) {
				# check user password
				if ( wp_check_password( $_POST['data']['password'], $current_user->data->user_pass, $current_user->ID ) ) {
					referral__add_withdraw_request( $_POST['data']['amount'], $_POST['data']['type'], $_POST['data']['data'] );

					$result['status']  = true;
					$result['msg'] = 'Withdraw Request Sent.';
				} else {
					$result['msg'] = 'Wrong Password.';
				}
			} else {
				$result['msg'] = 'Withdraw request was already sent.';
			}
		}
		else {
			$result['msg'] = 'Wrong Params.';
		}
	}

	wp_send_json( $result );
	die();
}

/**
 * Update referral withdraw request
 */
add_action('wp_ajax_referral_ajax__update_withdraw_request', 'referral_ajax__update_withdraw_request');
add_action('wp_ajax_nopriv_referral_ajax__update_withdraw_request', 'referral_ajax__update_withdraw_request');

function referral_ajax__update_withdraw_request () {
	$result = array(
		'status' => FALSE,
		'msg' => 'Error'
	);

	if( is_user_logged_in() && current_user_can('administrator') ) {
		if( isset( $_POST['data'] ) ) {
			# get withdraw request
			$withdraw_request = referral__get_withdraw_request( $_POST['data']['id'] );

			# check is old request status is PAID we can not update it and change
			if( $withdraw_request->status != REFERRAL__WITHDRAW_PAID ) {
				referral__update_withdraw_request( $_POST['data']['id'], $_POST['data']['status'], $withdraw_request );

				$result['status']  = true;
				$result['msg'] = 'Updated.';
			}
			else {
				$result['msg'] = 'Cannot update request, it was already closed and marked PAID.';
			}
		}
		else {
			$result['msg'] = 'Wrong Params.';
		}
	}

	wp_send_json( $result );
	die();
}

/**
 * Update user fields
 */
add_action('wp_ajax_referral_ajax__update_user_field', 'referral_ajax__update_user_field');
add_action('wp_ajax_nopriv_referral_ajax__update_user_field', 'referral_ajax__update_user_field');

function referral_ajax__update_user_field() {
	$result = array(
		'status' => FALSE,
		'msg' => 'Error'
	);

	if( is_user_logged_in() && current_user_can('administrator') ) {
		if( isset( $_POST['data']['user_id'], $_POST['data']['value'], $_POST['data']['name'] ) ) {
			referral__update_user_fields( $_POST['data']['user_id'], $_POST['data']['name'], $_POST['data']['value'] );

			$result['status']  = true;
			$result['msg'] = 'Updated.';
		}
		else {
			$result['msg'] = 'Wrong Params.';
		}
	}

	wp_send_json( $result );
	die();
}

/**
 * Update user fields
 */
add_action('wp_ajax_referral_ajax__block_unlock_user', 'referral_ajax__block_unlock_user');
add_action('wp_ajax_nopriv_referral_ajax__block_unlock_user', 'referral_ajax__block_unlock_user');

function referral_ajax__block_unlock_user() {
	$result = array(
		'status' => FALSE,
		'msg' => 'Error'
	);

	if( is_user_logged_in() && current_user_can('administrator') ) {
		if( isset( $_POST['data']['user_id'], $_POST['data']['type'] ) ) {
			referral__block_unlock_user( $_POST['data']['user_id'], $_POST['data']['type'] );

			$result['status']  = true;
			$result['msg'] = 'Blocked.';
			if( !$_POST['data']['type'] ) {
				$result['msg'] = 'Unlocked.';
			}
		}
		else {
			$result['msg'] = 'Wrong Params.';
		}
	}

	wp_send_json( $result );
	die();
}