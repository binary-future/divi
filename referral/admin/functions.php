<?php

add_action( 'admin_enqueue_scripts', function() {
	wp_enqueue_style( 'referral_admin_styles', REF__HOME_DIR . '/referral/admin/assets/css/referral-admin.css', '', '1.0' );
	wp_enqueue_style( 'referral_select_styles', REF__HOME_DIR . '/referral/admin/assets/css/selectize.default.css', '', '1.0' );

	?>
    <script>
        var ajaxUrl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
	<?php

    wp_enqueue_script( 'referral_select', REF__HOME_DIR . '/referral/admin/assets/js/selectize.min.js', array('jquery') );

	// Load script for front page template
	wp_enqueue_script( 'referral-notification', REF__HOME_DIR . '/assets/js/referral.notification.js', array( 'jquery' ));

	wp_enqueue_script( 'referral_admin_script', REF__HOME_DIR . '/referral/admin/assets/js/referral-admin.js' );
});

/**
 * Register referral pages
 */
add_action( 'admin_menu', function() {
	# Register dashboard admin page
	add_menu_page(
		'Referral',
		'Referral',
		'manage_options',
		'referral',
		'referral__dashboard',
		get_stylesheet_directory_uri() . '/assets/images/menu-referral.png',
		6
	);

	# Register withdraw admin page
	add_submenu_page(
		'referral',
		'Withdraw',
		'Withdraw',
		'manage_options',
		'referral__withdraw',
		'referral__withdraw'
	);

	# Register settings admin page
	add_submenu_page(
		'referral',
		'Settings',
		'Settings',
		'manage_options',
		'referral__settings',
		'referral__settings'
	);
});

function referral__statistic1() {
    global $wpdb;

    # chart 1
    $month1 = date('Y-m-01');
    $month1_name = date('M', strtotime($month1));

    $month2 = date('Y-m-d', strtotime(date('Y-m-01').' -1 MONTH'));
    $month2_name = date('M', strtotime($month2));

    $month3 = date('Y-m-d', strtotime(date('Y-m-01').' -2 MONTH'));
    $month3_name = date('M', strtotime($month3));

    $month4 = date('Y-m-d', strtotime(date('Y-m-01').' -3 MONTH'));
    $month4_name = date('M', strtotime($month4));

    $month5 = date('Y-m-d', strtotime(date('Y-m-01').' -4 MONTH'));
    $month5_name = date('M', strtotime($month5));

    $month6 = date('Y-m-d', strtotime(date('Y-m-01').' -5 MONTH'));

    $month1_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month1}' AND user_registered > '{$month2}'" );
    $month2_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month2}' AND user_registered > '{$month3}'" );
    $month3_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month3}' AND user_registered > '{$month4}'" );
    $month4_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month4}' AND user_registered > '{$month5}'" );
    $month5_count = $wpdb->get_var( "SELECT COUNT(*) FROM wpue_users WHERE user_registered < '{$month5}' AND user_registered > '{$month6}'" );

    # chart 2
    $withdraw_date = strtotime( date('Y-m-d').' -3 MONTH');
    $withdraw_requests = $wpdb->get_results( "SELECT * FROM " . REFERRAL__TABLE_WITHDRAW . " WHERE date > '{$withdraw_date}'" );

    $withdraw_in_process = 0;
    $withdraw_paid = 0;
    $withdraw_void = 0;
    $withdraw_sent = 0;

    foreach ( $withdraw_requests as $request ) {
        switch ( $request->status ) {
            case REFERRAL__WITHDRAW_IN_PROCESS :
                $withdraw_in_process++;
                break;
            case REFERRAL__WITHDRAW_PAID :
                $withdraw_paid++;
                break;
            case REFERRAL__WITHDRAW_VOID :
                $withdraw_void++;
                break;
            case REFERRAL__WITHDRAW_SENT :
                $withdraw_sent++;
                break;
        }
    }

    # chart 3
    $leads_last_date = strtotime($month6);
    $leads_data = $wpdb->get_results( "SELECT * FROM " . REFERRAL__TABLE_LEADS . " WHERE referred_date > '{$leads_last_date}'" );
    $leads = array(
        $month1 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 ),
        $month2 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 ),
        $month3 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 ),
        $month4 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 ),
        $month5 => array( REFERRAL__LEAD_EXPIRED => 0, REFERRAL__LEAD_SENT => 0, REFERRAL__LEAD_PENDING => 0, REFERRAL__LEAD_ELIGIBLE => 0 )
    );

    foreach ( $leads_data as $lead ) {
        # get lead month
        $month = date('Y-m-01', $lead->referred_date);

        $leads[$month][$lead->status]++;
    }

    $total_leads = $wpdb->get_var( "SELECT COUNT(*) FROM " . REFERRAL__TABLE_LEADS );

    ?>
    <div class="statistic__block">
        <h3 class="statistic__block-title">Users Registered</h3>
        <div class="statistic__block-info">
            <h4><?php echo count_users()['total_users']; ?></h4>
            <p>Total users</p>
        </div>
        <div id="statistic1-legend" class="statistic-legend"></div>
        <canvas id="statistic1" height="249" width="350"></canvas>
    </div>
    <div class="statistic__block">
        <h3 class="statistic__block-title">Withdraw request for last 3 months</h3>
        <div class="statistic__block-info">
            <h4><?php echo ($withdraw_in_process + $withdraw_paid + $withdraw_void + $withdraw_sent); ?></h4>
            <p>Total requests</p>
        </div>
        <div id="statistic2-legend" class="statistic-legend"></div>
        <canvas id="statistic2" height="249" width="350"></canvas>
    </div>
    <div class="statistic__block">
        <h3 class="statistic__block-title">Users Leads</h3>
        <div class="statistic__block-info">
            <h4><?php echo $total_leads; ?></h4>
            <p>Total leads</p>
        </div>
        <div id="statistic3-legend" class="statistic-legend"></div>
        <canvas id="statistic3" height="214" width="350"></canvas>
    </div>
    <script>
        jQuery(document).ready(function ($) {

            if( $('#statistic1').length ) {
                let statistic1 = new Chart(document.getElementById('statistic1').getContext('2d'), {
                    type: 'line',
                    data: {
                        labels: ['<?=$month5_name?>', '<?=$month4_name?>', '<?=$month3_name?>', '<?=$month2_name?>', '<?=$month1_name?>'],
                        datasets: [{
                            label: 'Users',
                            backgroundColor: "rgba(98, 187, 0, 0.7)",
                            borderColor: "rgba(98, 187, 0, 0.7)",
                            data: [<?=$month5_count?>, <?=$month4_count?>, <?=$month3_count?>, <?=$month2_count?>, <?=$month1_count?>],
                            fill: false,
                            pointRadius: 5,
                            pointHoverRadius: 5,
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                gridLines: {
                                    display: true
                                },
                                scaleLabel: {
                                    display: false
                                }
                            }],
                            yAxes: [{
                                display: true
                            }]
                        },
                        layout: {
                            padding: 20
                        }
                    }
                });

                $('#statistic1-legend').html( statistic1.generateLegend() );
            }

            if( $('#statistic2').length ) {
                new Chart(document.getElementById('statistic2').getContext('2d'), {
                    type: 'bar',
                    data: {
                        labels: ["In Process","Paid","Void","Sent"],
                        datasets:[{
                            data: [ <?=$withdraw_in_process?>, <?=$withdraw_paid?>, <?=$withdraw_void?>, <?=$withdraw_sent?> ],
                            fill: false,
                            backgroundColor: [
                                "rgba(139, 52, 215, 0.3)",
                                "rgba(98, 187, 0, 0.3)",
                                "rgba(255, 102, 121, 0.3)",
                                "rgba(63, 160, 248, 0.3)"
                            ],
                            borderColor: [
                                "#8B34D7",
                                "#62BB00",
                                "#FF6679",
                                "#3FA0F8"
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes:[{
                                ticks: {
                                    beginAtZero: true
                                },
                                gridLines: {
                                    display: true
                                }
                            }],
                            xAxes: [{
                                display: true
                            }],
                        },
                        layout: {
                            padding: 20
                        }
                    }
                });
            }

            if( $('#statistic3').length ) {
                let statistic3 = new Chart(document.getElementById('statistic3').getContext('2d'), {
                    type: 'line',
                    data: {
                        labels: ['<?=$month5_name?>', '<?=$month4_name?>', '<?=$month3_name?>', '<?=$month2_name?>', '<?=$month1_name?>'],
                        datasets: [{
                            label: 'Expired',
                            backgroundColor: "#8B34D7",
                            borderColor: "#8B34D7",
                            data: [<?=$leads[$month5][REFERRAL__LEAD_EXPIRED]?>, <?=$leads[$month4][REFERRAL__LEAD_EXPIRED]?>, <?=$leads[$month3][REFERRAL__LEAD_EXPIRED]?>, <?=$leads[$month2][REFERRAL__LEAD_EXPIRED]?>, <?=$leads[$month1][REFERRAL__LEAD_EXPIRED]?>],
                            fill: false,
                            pointRadius: 5,
                            pointHoverRadius: 5,
                        }, {
                            label: 'Sent',
                            backgroundColor: "#62BB00",
                            borderColor: "#62BB00",
                            data: [<?=$leads[$month5][REFERRAL__LEAD_SENT]?>, <?=$leads[$month4][REFERRAL__LEAD_SENT]?>, <?=$leads[$month3][REFERRAL__LEAD_SENT]?>, <?=$leads[$month2][REFERRAL__LEAD_SENT]?>, <?=$leads[$month1][REFERRAL__LEAD_SENT]?>],
                            fill: false,
                            pointRadius: 5,
                            pointHoverRadius: 5,
                        }, {
                            label: 'Pending',
                            backgroundColor: "#FF6679",
                            borderColor: "#FF6679",
                            data: [<?=$leads[$month5][REFERRAL__LEAD_PENDING]?>, <?=$leads[$month4][REFERRAL__LEAD_PENDING]?>, <?=$leads[$month3][REFERRAL__LEAD_PENDING]?>, <?=$leads[$month2][REFERRAL__LEAD_PENDING]?>, <?=$leads[$month1][REFERRAL__LEAD_PENDING]?>],
                            fill: false,
                            pointRadius: 5,
                            pointHoverRadius: 5,
                        }, {
                            label: 'Eligible',
                            backgroundColor: "#3FA0F8",
                            borderColor: "#3FA0F8",
                            data: [<?=$leads[$month5][REFERRAL__LEAD_ELIGIBLE]?>, <?=$leads[$month4][REFERRAL__LEAD_ELIGIBLE]?>, <?=$leads[$month3][REFERRAL__LEAD_ELIGIBLE]?>, <?=$leads[$month2][REFERRAL__LEAD_ELIGIBLE]?>, <?=$leads[$month1][REFERRAL__LEAD_ELIGIBLE]?>],
                            fill: false,
                            pointRadius: 5,
                            pointHoverRadius: 5,
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                gridLines: {
                                    display: true
                                },
                                scaleLabel: {
                                    display: false
                                }
                            }],
                            yAxes: [{
                                display: true
                            }]
                        },
                        layout: {
                            padding: 20
                        }
                    }
                });

                $('#statistic3-legend').html( statistic3.generateLegend() );
            }
        });
    </script>
    <?php
}

/**
 * Referral Dashboard
 */
function referral__dashboard() {
	# current admin page url
	$page_url = admin_url( 'admin.php?page=referral' );

	# If user ID not set show dashboard page
	if ( ! isset( $_GET['user_id'] ) ) {
		$current_page = 1;
		if ( isset( $_GET['ref-page'] ) ) {
			$current_page = $_GET['ref-page'];
		}

		$order_by = 'ID';
		$order    = 'DESC';
		if ( isset( $_GET['order_by'] ) ) {
			$order_by = $_GET['order_by'];
		}

		$arrow_direction = '';
		if ( isset( $_GET['order'] ) ) {
			if ( $_GET['order'] === 'DESC' ) {
				$order           = 'ASC';
				$arrow_direction = 'up';
			} else {
				$order           = 'DESC';
				$arrow_direction = 'down';
			}
		}

        $search_val = '';
        if( isset( $_GET['s'] ) ) {
            $search_val = $_GET['s'];
        }

		$url_name       = "<a href='{$page_url}&order_by=name&order=DESC&s={$search_val}'>Name</a>";
		$url_email      = "<a href='{$page_url}&order_by=email&order=DESC&s={$search_val}'>Email</a>";
		$url_paid       = "<a href='{$page_url}&order_by=paid&order=DESC&s={$search_val}'>Paid</a>";
		$url_balance    = "<a href='{$page_url}&order_by=balance&order=DESC&s={$search_val}'>Balance</a>";
        $url_pending    = "<a href='{$page_url}&order_by=pending&order=DESC&s={$search_val}'>Pending</a>";
        $url_commission = "<a href='{$page_url}&order_by=commission&order=DESC&s={$search_val}'>Commission</a>";
        $url_referrals  = "<a href='{$page_url}&order_by=invitations_total&order=DESC&s={$search_val}'>Referrals#</a>";

		if ( $order_by == 'name' ) {
			$url_name = "<a href='{$page_url}&order_by=name&order={$order}&s={$search_val}'><span class='dashicons-before dashicons-arrow-{$arrow_direction}'></span>Name</a>";
			$order_by = 'users.display_name';
		}
		else if ( $order_by == 'email' ) {
			$url_email = "<a href='{$page_url}&order_by=email&order={$order}&s={$search_val}'><span class='dashicons-before dashicons-arrow-{$arrow_direction}'></span>Email</a>";
			$order_by  = 'users.user_email';
		}
		else if ( $order_by == 'paid' ) {
			$url_paid = "<a href='{$page_url}&order_by=paid&order={$order}&s={$search_val}'><span class='dashicons-before dashicons-arrow-{$arrow_direction}'></span>Paid</a>";
			$order_by = 'earning';
		}
		else if ( $order_by == 'balance' ) {
			$order_by    = 'balance';
			$url_balance = "<a href='{$page_url}&order_by=balance&order={$order}&s={$search_val}'><span class='dashicons-before dashicons-arrow-{$arrow_direction}'></span>Balance</a>";
		}
        else if ( $order_by == 'pending' ) {
            $order_by    = 'withdraw.amount';
            $url_pending = "<a href='{$page_url}&order_by=pending&order={$order}&s={$search_val}'><span class='dashicons-before dashicons-arrow-{$arrow_direction}'></span>Pending</a>";
        }
        else if ( $order_by == 'commission' ) {
            $order_by    = 'commission';
            $url_commission = "<a href='{$page_url}&order_by=commission&order={$order}&s={$search_val}'><span class='dashicons-before dashicons-arrow-{$arrow_direction}'></span>Commission</a>";
        }
        else if ( $order_by == 'invitations_total' ) {
            $order_by    = 'invitations_total';
            $url_referrals = "<a href='{$page_url}&order_by=invitations_total&order={$order}&s={$search_val}'><span class='dashicons-before dashicons-arrow-{$arrow_direction}'></span>Referrals#</a>";
        }

        $order_direction = '';
        if( isset($_GET['order']) ) {
            $order_direction = $_GET['order'];
        }

		$users = referral__get_users( ( $current_page - 1 ) * 20, $order_by, $order, $search_val );

		?>
        <div class="referral statistic clearfix">
            <?php referral__statistic1(); ?>
        </div>
        <div class="referral dashboard">
            <h2 class="title">Users</h2>
            <div class="referral__search">
                <form>
                    <input type="hidden" name="page" value="<?=$_GET['page']?>">
                    <input type="hidden" name="order_by" value="<?=$order_by?>">
                    <input type="hidden" name="order" value="<?=$order_direction?>">

                    <input name="s" type="search" placeholder="Type Name or Email..." value="<?=$search_val?>">
                    <button class="referral__search-run" type="submit">Find</button>
                </form>
            </div>
            <?php if( count($users) ) : ?>
                <table class="referral__table">
                    <thead>
                    <tr>
                        <td class="user_id">User Id</td>
                        <td class="name"><?php echo $url_name; ?></td>
                        <td class="email"><?php echo $url_email; ?></td>
                        <td class="referrals-used">Referrals Used</td>
                        <td class="referrals-remaining">Referrals Remaining</td>
                        <td class="pending"><?php echo $url_pending; ?></td>
                        <td class="balance"><?php echo $url_balance; ?></td>
                        <td class="paid"><?php echo $url_paid; ?></td>
                        <td class="commission"><?php echo $url_commission; ?></td>
                        <td class="referrals"><?php echo $url_referrals; ?></td>
                        <td class="options">Options</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ( $users as $user ) {
                        ?>
                        <tr>
                            <td class="user_id"><?php echo $user->user_id; ?></td>
                            <td class="name"><?php echo $user->display_name; ?></td>
                            <td class="email"><?php echo $user->user_email; ?></td>
                            <td class="referrals-used"><?php echo $user->invitations_used; ?></td>
                            <td class="referrals-remaining"><?php echo $user->invitations_total - $user->invitations_used; ?></td>
                            <td class="pending">$ <?php if( empty($user->pending) ) { echo '0'; } else { echo $user->pending; } ?></td>
                            <td class="balance">$ <?php echo $user->balance; ?></td>
                            <td class="paid">$ <?php echo $user->earning; ?></td>
                            <td class="commission">
                            <span class="referral__td-value">
                                <span>
                                    $ <?php echo $user->commission; ?>
                                </span>
                                <a href="#" class="edit dashicons-before dashicons-edit"></a>
                            </span>
                                <span class="referral__td-input">
                                <input type="text" value="<?php echo $user->commission; ?>">
                                <a href="#" class="save dashicons-before dashicons-yes"></a>
                            </span>
                            </td>
                            <td class="referrals">
                            <span class="referral__td-value">
                                <span>
                                    <?php echo $user->invitations_total; ?>
                                </span>
                                <a href="#" class="edit dashicons-before dashicons-edit"></a>
                            </span>
                                <span class="referral__td-input">
                                <input type="text" value="<?php echo $user->invitations_total; ?>">
                                <a href="#" class="save dashicons-before dashicons-yes"></a>
                            </span>
                            </td>
                            <td class="options">
                                <div class="referral__btns">
                                    <a href="<?php echo $page_url . '&user_id=' . $user->user_id; ?>">Details</a>
                                    <?php
                                    $btn_text  = 'Block';
                                    $btn_class = 'referral__btn-block';
                                    if ( $user->blocked ) {
                                        $btn_text  = 'Unlock';
                                        $btn_class = 'referral__btn-unlock';
                                    }
                                    ?>
                                    <a href="#" class="error <?php echo $btn_class; ?>"
                                       data-referral-id="<?php echo $user->user_id; ?>"><?php echo $btn_text; ?></a>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>

                <div class="referral__pagination">
                    <?php
                    $requests_count = referral__get_users_count($search_val);

                    $prev_url = '';
                    $next_url = '';

                    if ( $current_page - 1 > 0 ) {
                        $prev_url = $page_url . '&ref-page=' . ( $current_page - 1 );
                    }

                    if ( $current_page + 1 <= ceil( $requests_count / 20 ) ) {
                        $next_url = $page_url . '&ref-page=' . ( $current_page + 1 );
                    }

                    ?>
                    <a href="<?php echo $prev_url; ?>"><</a>
                    <?php

                    for ( $i = 0; $requests_count / 20 > $i; $i ++ ) {
                        if( $current_page + 4 > $i && $current_page - 6 < $i ) {
                            $current = '';
                            if ($current_page == $i + 1) {
                                $current = 'current';
                            }
                            echo "<a href='" . $page_url . '&ref-page=' . ($i + 1) . "&order_by={$order_by}&order={$order_direction}&s={$search_val}' class='{$current}'>" . ($i + 1) . "</a>";
                        }
                        else if ( $i == 0 ) {
                            echo "<a href='" . $page_url . '&ref-page=' . ($i + 1) . "&order_by={$order_by}&order={$order_direction}&s={$search_val}' class=''>" . ($i + 1) . "</a> ... ";
                        }
                        else if( $i == (ceil($requests_count / 20)-1) ) {
                            echo " ... <a href='" . $page_url . '&ref-page=' . ($i + 1) . "&order_by={$order_by}&order={$order_direction}&s={$search_val}' class=''>" . ($i + 1) . "</a>";
                        }
                    }
                    ?>
                    <a href="<?php echo $next_url; ?>">></a>
                </div>
            <?php else : ?>
                <h3 class="sub-title referral__not-found">No users found...</h3>
            <?php endif; ?>
        </div>
		<?php
	} else {
		referral__single_user_page( $_GET['user_id'] );
	}
}

/**
 * Referral withdraw page
 */
function referral__withdraw() {
	$current_page = 1;
	if( isset( $_GET['ref-page'] ) ) {
		$current_page = $_GET['ref-page'];
	}

	$requests = referral__get_withdraw_requests( ($current_page-1)*20 );
	?>
	<div class="referral withdraw">
		<h2 class="title">Withdraw requests</h2>
		<h3 class="sub-title">Users</h3>
		<table class="referral__table">
            <thead>
            <tr>
                <td class="user_id">User Id</td>
                <td class="name">Name</td>
                <td class="email">Email</td>
                <td class="amount">Amount</td>
                <td class="payment-system">Payment system</td>
                <td class="status">Status</td>
                <td class="date">Date</td>
                <td class="options">Options</td>
            </tr>
            </thead>
            <tbody>
			<?php
            $statuses = array(
                1 => 'In Process',
                2 => 'Paid',
                3 => 'Void',
                4 => 'Sent'
            );
			foreach ( $requests as $request ) {
				?>
                <tr data-payment-data="<?php echo $request->data; ?>" data-request-id="<?php echo $request->ID; ?>">
                    <td class="user_id">
                        <a href="<?php echo site_url(); ?>/wp-admin/user-edit.php?user_id=<?php echo $request->user_id; ?>" target="_blank">
	                        <?php echo $request->user_id; ?>
                        </a>
                    </td>
                    <td class="name">
                        <a href="<?php echo site_url(); ?>/wp-admin/user-edit.php?user_id=<?php echo $request->user_id; ?>" target="_blank">
	                        <?php echo $request->display_name; ?>
                        </a>
                    </td>
                    <td class="email"><?php echo $request->user_email; ?></td>
                    <td class="amount">$ <?php echo $request->amount; ?></td>
                    <td class="payment-system">
                        <?php echo "<img src='" . REF__HOME_DIR . "/assets/images/{$request->type}.png' alt='{$request->type}'>"; ?>
                    </td>
                    <td class="status">
                        <select class="selectize" data-request-id="<?php echo $request->ID; ?>">
                            <?php
                            foreach ($statuses as $key => $status) {
                                $selected = '';
                                if( $key == $request->status ) {
                                    $selected = 'selected';
                                }
                                echo "<option value='{$key}' {$selected}>{$status}</option>";
                            }
                            ?>
                        </select>
                    </td>
                    <td class="date"><?php echo date('m/d/Y', $request->date); ?></td>
                    <td class="options">
                        <div class="referral__btns">
                            <a href="#" class="details <?php echo $request->type; ?>">Details</a>
	                        <?php
	                        $btn_text = 'Block';
	                        $btn_class = 'referral__btn-block';
	                        if( $request->blocked ) {
		                        $btn_text = 'Unlock';
		                        $btn_class = 'referral__btn-unlock';
	                        }
	                        ?>
                            <a href="#" class="error <?php echo $btn_class; ?>" data-referral-id="<?php echo $request->user_id; ?>"><?php echo $btn_text; ?></a>
                        </div>
                    </td>
                </tr>
				<?php
			}
			?>
            </tbody>
		</table>

        <div class="referral__pagination">
            <?php

            $requests_count = referral__get_withdraw_requests_count();

            $prev_url = '';
            $next_url = '';

            if( $current_page-1 > 0 ) {
	            $prev_url = admin_url('admin.php?page=referral__withdraw&ref-page=' . ($current_page-1));
            }

            if( $current_page+1 <= ceil($requests_count/20) ) {
	            $next_url = admin_url('admin.php?page=referral__withdraw&ref-page=' . ($current_page+1));
            }

            ?>
            <a href="<?php echo $prev_url; ?>"><</a>
            <?php

            for( $i = 0; $requests_count/20 > $i; $i++ ) {
                $current = '';
                if( $current_page == $i+1 ) {
                    $current = 'current';
                }
                echo "<a href='" . admin_url('admin.php?page=referral__withdraw&ref-page=' . ($i+1)) .  "' class='{$current}'>" . ($i+1) . "</a>";
            }
            ?>
            <a href="<?php echo $next_url; ?>">></a>
        </div>

        <div class="popup">
            <div class="popup__inner">
                <div class="popup__container">
                    <div class="popup__img">
                        <img src="<?php echo REF__HOME_DIR . "/assets/images/paypal-big.png"; ?>" alt="paypal" class="popup__img-paypal" style="display: none;">
                        <img src="<?php echo REF__HOME_DIR . "/assets/images/bank-transfer.png"; ?>" alt="bank transfer" class="popup__img-bank" style="display: none;">
                    </div>
                    <h3 class="popup__title">Payment Details:</h3>
                    <p class="popup__details"></p>
                    <select class="selectize popup-select" data-request-id="">
                        <option value="default" disabled>Choose Status...</option>
		                <?php
		                foreach ($statuses as $key => $status) {
			                echo "<option value='{$key}'>{$status}</option>";
		                }
		                ?>
                    </select>
                </div>
            </div>
        </div>
	</div>
	<?php
}

/**
 * Referral settings page
 */
function referral__settings() {
	?>
	<div class="referral">
		<h2 class="title">Settings</h2>
		<div class="settings">
			<h4 class="settings__title">Default commission</h4>
			<div class="settings__input">
				<span class="settings__dollar">$</span>
				<input type="text" name="commission" class="small" value="<?php echo referral__settings_get('commission'); ?>">
			</div>
		</div>
		<div class="settings">
			<h4 class="settings__title">Default # of referrals</h4>
			<div class="settings__input">
				<input type="text" name="referrals" class="small" value="<?php echo referral__settings_get('referrals'); ?>">
			</div>
		</div>
		<div class="settings">
			<h4 class="settings__title">Default email Subject Line</h4>
			<div class="settings__input">
				<input type="text" name="email-subject" placeholder="Default Subject Line" value="<?php echo referral__settings_get('email-subject'); ?>">
			</div>
		</div>
		<div class="settings">
			<h4 class="settings__title">Default email Body Text</h4>
			<div class="settings__editor">
				<?php wp_editor( referral__settings_get('email-body-text'), 'email-body-text', $settings = array('media_buttons' => false, 'editor_height'=>'300') ); ?>
			</div>
		</div>
		<h2 class="title">Follow up email (Reminder To Person) email Subject Line & Body Text</h2>
		<div class="settings">
			<h4 class="settings__title">Subject Line</h4>
			<div class="settings__input">
				<input type="text" name="reminder-person" placeholder="Reminder To Person" value="<?php echo referral__settings_get('reminder-person'); ?>">
			</div>
		</div>
		<div class="settings">
			<h4 class="settings__title">Body Text</h4>
			<div class="settings__editor">
				<?php wp_editor( referral__settings_get('person-email-body-text'), 'person-email-body-text', $settings = array('media_buttons' => false, 'editor_height'=>'300') ); ?>
			</div>
		</div>
		<h2 class="title">Follow up email (Reminder To User) email Subject Line & Body Text</h2>
		<div class="settings">
			<h4 class="settings__title">Subject Line</h4>
			<div class="settings__input">
				<input type="text" name="reminder-user" placeholder="Reminder To User" value="<?php echo referral__settings_get('reminder-user'); ?>">
			</div>
		</div>
		<div class="settings">
			<h4 class="settings__title">Body Text</h4>
			<div class="settings__editor">
				<?php wp_editor( referral__settings_get('user-email-body-text'), 'user-email-body-text', $settings = array('media_buttons' => false, 'editor_height'=>'300') ); ?>
			</div>
		</div>
		<div class="referral__buttons">
			<a href="#" class="save-button">Save Settings</a>
		</div>
	</div>
    <p class="referral__message"></p>
	<?php
}

/**
 * Dashboard single user page
 *
 * @param $user_id
 */
function referral__single_user_page($user_id) {
    $referral_user = referral__get_user( $user_id );

    ?>
    <div class="referral single-user">
        <h2 class="title"><?php echo $referral_user->user_email; ?></h2>
        <h3 class="sub-title">Users Leads</h3>

        <table class="referral__table">
            <thead>
            <tr>
                <td class="name">Name</td>
                <td class="email">Email</td>
                <td class="referred">Referred</td>
                <td class="expires">Expires</td>
                <td class="status">Status</td>
<!--                <td class="options">Options</td>-->
            </tr>
            </thead>
            <tbody>
		    <?php
		    foreach ( referral__get_user_leads($user_id) as $lead ) {
			    $expired_date = $lead->referred_date + ( 60 * 60 * 24 * REFERRAL__TO_PENDING_DAYS );
			    $status = referral__get_status_text($lead->status);
			    ?>
                <tr>
                    <td class="name"><?php echo $lead->name; ?></td>
                    <td class="email"><?php echo $lead->email ?></td>
                    <td class="referred"><?php echo date('m/d/Y', $lead->referred_date); ?></td>
                    <td class="expires"><?php echo date('m/d/Y', $expired_date); ?></td>
                    <td class="status <?php echo $status; ?>"><?php echo $status; ?></td>
<!--                    <td class="options">-->
<!--                        <div class="referral__btns">-->
<!--                            <a href="#" class="error referral__btn-block" data-referral-id="8095">Block</a>-->
<!--                        </div>-->
<!--                    </td>-->
                </tr>
			    <?php
		    }
		    ?>
            </tbody>
        </table>
        <div class="referral__buttons">
            <a href="<?php echo admin_url('admin.php?page=referral'); ?>">Back To Dashboard</a>
        </div>
    </div>
    <?php
}