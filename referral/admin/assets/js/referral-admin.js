jQuery(document).ready(function ($) {
    $('.save-button').click(function (e) {
        e.preventDefault();

        $('.referral__message').css('bottom', '20px').text('Saving...');

        var settingsData = {
            'action': 'referral_ajax__save_settings',
            'dataType': 'JSON',
            'data': {
                'commission'             : $('[name="commission"]').val(),
                'referrals'              : $('[name="referrals"]').val(),
                'email-subject'          : $('[name="email-subject"]').val(),
                'email-body-text'        : tinyMCE.get("email-body-text").getContent(),
                'reminder-person'        : $('[name="reminder-person"]').val(),
                'person-email-body-text' : tinyMCE.get("person-email-body-text").getContent(),
                'reminder-user'          : $('[name="reminder-user"]').val(),
                'user-email-body-text'   : tinyMCE.get("user-email-body-text").getContent()
            }
        };

        jQuery.post( ajaxUrl, settingsData, function(response) {
            $('.referral__message').text(response).css({'left':'calc(50% - ' + $('.referral__message').width()/2 + 'px)'});
            setTimeout(function () {
                $('.referral__message').removeAttr('style');
            }, 5000);
        });
    });

    $('td.status .selectize').selectize();

    // initialize the Selectize control
    var $popupSelect = $('.selectize.popup-select').selectize();

    var popupSelect = null;
    try {
        // fetch the instance
        popupSelect = $popupSelect[0].selectize;
    }
    catch (e) {

    }

    $('.details').click(function (e) {
        e.preventDefault();

        if( $(this).hasClass('paypal') ) {
            $('.popup__img-paypal').show();
            $('.popup__img-bank').hide();
        }
        else {
            $('.popup__img-bank').show();
            $('.popup__img-paypal').hide();
        }

        var parentTr = $(this).parents('tr');
        $('.popup__details').text( parentTr.attr('data-payment-data') );
        popupSelect.clear(true);
        $('.popup-select').attr('data-request-id', parentTr.attr('data-request-id') );

        $('.popup').fadeIn();
    });

    $('.popup__buttons a.popup__button-cancel, .popup').click(function (e) {
        if( this === e.target ) {
            e.preventDefault();
            $('.popup').fadeOut();
        }
    });

    $('td.status .selectize, .selectize.popup-select').change(function () {
        jQuery.referralNotification.show('Updating...');

        var _this = $(this);
        var requestId = _this.attr('data-request-id');
        var currentValue = _this.val();

        var postData = {
            'action': 'referral_ajax__update_withdraw_request',
            'dataType': 'JSON',
            'data': {
                'id': requestId,
                'status': currentValue
            }
        };

        jQuery.post(ajaxUrl, postData, function (response) {
            var messageClass = '';
            if (!response.status) {
                messageClass = 'error';
            }
            jQuery.referralNotification.show(response.msg, messageClass, true);

            if( response.status ) {
                $('.popup').fadeOut();
            }
        });
    });

    $('.referral__td-value .edit').click(function (e) {
        e.preventDefault();
        var parent = $(this).parents('td');
        parent.find('.referral__td-value').hide();
        parent.find('.referral__td-input').show();

        parent.find('input').select();
    });

    $('.referral__td-input .save').click(function (e) {
        e.preventDefault();

        jQuery.referralNotification.show('Updating...');

        var parent = $(this).parents('td');
        var value = parent.find('input').val();
        var parentTr = parent.parent();
        var user_id = parentTr.find('.user_id').text();

        parent.find('.referral__td-value').show();
        parent.find('.referral__td-input').hide();

        var postData = {
            'action': 'referral_ajax__update_user_field',
            'dataType': 'JSON',
            'data': {
                'user_id': user_id,
                'value': value
            }
        };

        if( parent.hasClass('commission') ) {
            parent.find('.referral__td-value span').text( '$ ' + value );
            postData.data.name = 'commission';
        }
        else if( parent.hasClass('referrals') ) {
            parent.find('.referral__td-value span').text( value );
            parentTr.find('.referrals-remaining').text( parseInt(value) - parseInt(parentTr.find('.referrals-used').text()) );

            parent.find('.referral__td-value span').text( value );
            postData.data.name = 'invitations_total';
        }

        jQuery.post(ajaxUrl, postData, function (response) {
            var messageClass = '';
            if (!response.status) {
                messageClass = 'error';
            }
            jQuery.referralNotification.show(response.msg, messageClass, true);
        });
    });

    // block/unlock referral users
    $(document).on('click', '.referral__btn-block, .referral__btn-unlock', function (e) {
        e.preventDefault();

        var _this = $(this);
        var userId = _this.attr('data-referral-id');
        var type = 1;
        if( _this.hasClass('referral__btn-unlock') ) {
            type = 0;
            jQuery.referralNotification.show('Unlocking...');
        }
        else {
            jQuery.referralNotification.show('Blocking...');
        }

        var postData = {
            'action': 'referral_ajax__block_unlock_user',
            'dataType': 'JSON',
            'data': {
                'user_id': userId,
                'type': type
            }
        };

        jQuery.post(ajaxUrl, postData, function (response) {
            var messageClass = '';
            if (!response.status) {
                messageClass = 'error';
            }
            if( type ) {
                _this.text('Unlock').addClass('referral__btn-unlock').removeClass('referral__btn-block');
            }
            else {
                _this.text('Block').removeClass('referral__btn-unlock').addClass('referral__btn-block');
            }
            jQuery.referralNotification.show(response.msg, messageClass, true);
        });
    });
});