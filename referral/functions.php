<?php

require 'settings.php';

/**
 * Get Referral Settings Item
 * @param $name
 *
 * @return null|string
 */
function referral__settings_get($name) {
	global $wpdb;

	$value = $wpdb->get_var( "SELECT value FROM " . REFERRAL__TABLE_SETTINGS . " WHERE name = '" . $name ."'" );
	return $value;
}

/**
 * Set Referral Settings Item
 * @param $name
 * @param $value
 */
function referral__settings_set( $name, $value ) {
	global $wpdb;

	$wpdb->update(
		REFERRAL__TABLE_SETTINGS,
		array( 'value' => $value ),
		array( 'name' => $name )
	);
}

/**
 * Add lead
 *
 * @param $email
 * @param $name
 * @param $current_user
 * @param $message
 * @param $invite_hash
 */
function referral__add_lead($email, $name, $current_user, $message, $invite_hash) {
	global $wpdb;

	# add lead to table
	$data = array(
		'user_id'       => get_current_user_id(),
		'email'         => $email,
		'name'          => $name,
		'referred_date' => time(),
		'invite_msg'    => $message,
		'invite_hash'   => $invite_hash,
		'status'        => REFERRAL__LEAD_SENT
	);

	$wpdb->insert( REFERRAL__TABLE_LEADS, $data );

	# update invitation used for current user
	$wpdb->update(
		REFERRAL__TABLE_USERS,
		array( 'invitations_used' => $current_user->invitations_used + 1 ),
		array( 'user_id' => $current_user->user_id )
	);
}

/**
 * Check is lead exist
 * @param $lead_email
 *
 * @return bool
 */
function referral__is_lead_exist($lead_email) {
	global $wpdb;

	$value = $wpdb->get_var( "SELECT ID FROM " . REFERRAL__TABLE_LEADS . " WHERE email = '" . $lead_email ."'" );

	return !empty($value);
}

/**
 * Check is user exist
 * @param $user_email
 *
 * @return bool
 */
function referral__is_user_exist($user_email) {
	if ( ! empty( get_user_by('email', $user_email) ) ) {
		return TRUE;
	}
	return FALSE;
}


/**
 * Check is current user was added to referral system
 *
 * @return bool
 */
function referral__is_user_added() {
	global $wpdb;

	$value = $wpdb->get_var( "SELECT ID FROM " . REFERRAL__TABLE_USERS . " WHERE user_id = '" . get_current_user_id() ."'" );
	if( empty($value) ) {
	    return FALSE;
    }
	return TRUE;
}

# if current user was not added to referral system do it
if( is_user_logged_in() && !referral__is_user_added() ) {
	global $wpdb;

	$wpdb->insert(
		REFERRAL__TABLE_USERS,
		array(
            'user_id' => get_current_user_id(),
            'commission' => referral__settings_get('commission'),
            'earning' => 0,
            'balance' => 0,
            'invitations_total' => referral__settings_get('referrals'),
            'invitations_used' => 0,
            'blocked' => FALSE
        )
	);
}

/**
 * Get referral user
 *
 * @param null $user_id
 *
 * @return object
 */
function referral__get_user($user_id = null) {
	global $wpdb;

	if( $user_id == null ) {
	    $user_id = get_current_user_id();
    }

	$user = $wpdb->get_row( "
        SELECT table_user.*, users.user_email, users.display_name, withdraw.amount as 'pending'
        FROM " . REFERRAL__TABLE_USERS . " table_user 
        LEFT JOIN {$wpdb->prefix}users users ON table_user.user_id = users.ID 
        LEFT JOIN " . REFERRAL__TABLE_WITHDRAW . " withdraw ON withdraw.user_id = table_user.user_id 
        AND (withdraw.status = ". REFERRAL__WITHDRAW_IN_PROCESS . " OR withdraw.status = " . REFERRAL__WITHDRAW_SENT . ")
        WHERE table_user.user_id = {$user_id}
    ");

	return $user;
}

/**
 * Update user amount
 *
 * @param $value
 * @param bool $is_increment
 * @param null $user_id
 */
function referral__update_user_amount($value, $is_increment = false, $user_id = null) {
	global $wpdb;

	if ( $user_id == null ) {
		$user_id = get_current_user_id();
	}

	# get user object
	$user_obj = referral__get_user( $user_id );

	# if true increment amount
	if ( $is_increment ) {
	    $new_amount = $user_obj->balance + $value;
	}
	# else decrement amount
	else {
		$new_amount = $user_obj->balance - $value;
    }

	$wpdb->update(
		REFERRAL__TABLE_USERS,
		array(
			'balance' => $new_amount
		),
		array(
			'user_id' => $user_id
		)
	);
}

/**
 * Get user leads count
 *
 * @return null|string
 */
function referral__get_user_leads_count() {
	global $wpdb;

	$leads_count = $wpdb->get_var( "SELECT COUNT(*) FROM " . REFERRAL__TABLE_LEADS . " WHERE user_id = '" . get_current_user_id() ."'" );

	return $leads_count;
}

/**
 * Get user leads count
 *
 * @return null|string
 */
function referral__get_user_success_leads_count() {
	global $wpdb;

	$leads_count = $wpdb->get_var( "SELECT COUNT(*) FROM " . REFERRAL__TABLE_LEADS . " WHERE user_id = '" . get_current_user_id() ."' AND status = '" . REFERRAL__LEAD_PENDING . "' OR status = '" . REFERRAL__LEAD_ELIGIBLE . "'" );

	return $leads_count;
}

/**
 * Get user leads
 *
 * @param null $user_id
 *
 * @return array|null|object
 */
function referral__get_user_leads($user_id = null) {
    global $wpdb;

    if( $user_id == null ) {
        $user_id = get_current_user_id();
    }

    $leads = $wpdb->get_results("SELECT * FROM " . REFERRAL__TABLE_LEADS . " WHERE user_id = '{$user_id}'");

    return $leads;
}

/**
 * Get referrals page sidebar
 */
function referral__get_user_sidebar() {
	$current_url = get_permalink();
	?>
	<div class="referral__sidebar">
		<ul>
			<li>
				<?php
				$link = get_site_url( null, '/refer-friend/' );
				$class = '';
				if( $link == $current_url ) {
					$class = 'current';
				}
				?>
				<a href="<?php echo $link; ?>" class="refer-friend <?php echo $class; ?>">
					<span class="icon"><svg width="20px" height="16px" viewBox="0 0 20 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Lefn-Menu/Refer-a-Friend--" transform="translate(-32.000000, -12.000000)" fill="#B2B2B2" fill-rule="nonzero"><g id="user-plus" transform="translate(32.000000, 12.000000)"><path d="M19.5,6.5 L17.5,6.5 L17.5,4.5 C17.5,4.225 17.275,4 17,4 L16,4 C15.725,4 15.5,4.225 15.5,4.5 L15.5,6.5 L13.5,6.5 C13.225,6.5 13,6.725 13,7 L13,8 C13,8.275 13.225,8.5 13.5,8.5 L15.5,8.5 L15.5,10.5 C15.5,10.775 15.725,11 16,11 L17,11 C17.275,11 17.5,10.775 17.5,10.5 L17.5,8.5 L19.5,8.5 C19.775,8.5 20,8.275 20,8 L20,7 C20,6.725 19.775,6.5 19.5,6.5 Z M7,8 C9.209375,8 11,6.209375 11,4 C11,1.790625 9.209375,0 7,0 C4.790625,0 3,1.790625 3,4 C3,6.209375 4.790625,8 7,8 Z M9.8,9 L9.278125,9 C8.584375,9.31875 7.8125,9.5 7,9.5 C6.1875,9.5 5.41875,9.31875 4.721875,9 L4.2,9 C1.88125,9 0,10.88125 0,13.2 L0,14.5 C0,15.328125 0.671875,16 1.5,16 L12.5,16 C13.328125,16 14,15.328125 14,14.5 L14,13.2 C14,10.88125 12.11875,9 9.8,9 Z" id="Shape"></path></g></g></g></svg></span>
					<span class="text">Refer a Friend</span>
				</a>
			</li>
			<li>
				<?php
				$link = get_site_url( null, '/referral/' );
				$class = '';
				if( $link == $current_url ) {
					$class = 'current';
				}
				?>
				<a href="<?php echo $link; ?>" class="your-earnings <?php echo $class; ?>">
					<span class="icon"><svg width="22px" height="16px" viewBox="0 0 22 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Left-Menu/Your-Earnings--" transform="translate(-29.000000, -13.000000)" fill="#666666" fill-rule="nonzero"><g id="chart-line" transform="translate(29.000000, 13.000000)"><path d="M21.484375,13.3333333 C21.7679688,13.3333333 22,13.5583333 22,13.8333333 L22,15.5 C22,15.775 21.7679688,16 21.484375,16 L0.515625,16 C0.23203125,16 0,15.775 0,15.5 L0,0.5 C0,0.225 0.23203125,0 0.515625,0 L2.234375,0 C2.51796875,0 2.75,0.225 2.75,0.5 L2.75,13.3333333 L21.484375,13.3333333 Z M19.59375,1.33333333 L14.78125,1.33333333 C13.8617187,1.33333333 13.4019531,2.4125 14.0507812,3.04166667 L15.4644531,4.4125 L12.3707031,7.45 L9.98164062,5.13333333 C9.7796875,4.9375 9.45742187,4.9375 9.25546875,5.13333333 L4.1421875,10.0416667 C3.94023438,10.2333333 3.9359375,10.55 4.13359375,10.7458333 L5.35820313,11.9708333 C5.56015625,12.1708333 5.89101562,12.175 6.09296875,11.975 L9.62070312,8.55416667 L12.0054687,10.8666667 C12.2074219,11.0625 12.5339844,11.0625 12.7359375,10.8666667 L17.428125,6.31666667 L18.8632812,7.70833333 C19.5121094,8.3375 20.625,7.89166667 20.625,7 L20.625,2.33333333 C20.625,1.77916667 20.1652344,1.33333333 19.59375,1.33333333 Z" id="Shape"></path></g></g></g></svg></span>
					<span class="text">Your Earnings</span>
				</a>
			</li>
		</ul>
	</div>
	<?php
}

/**
 * Get referral status text from code
 * @param $status
 *
 * @return string
 */
function referral__get_status_text($status) {
    $text = '';
    switch ($status) {
	    case REFERRAL__LEAD_EXPIRED:
		    $text = 'expired'; break;
	    case REFERRAL__LEAD_SENT:
		    $text = 'sent'; break;
	    case REFERRAL__LEAD_PENDING:
		    $text = 'pending'; break;
	    case REFERRAL__LEAD_ELIGIBLE:
		    $text = 'eligible'; break;
    }

    return $text;
}

/**
 * Get user pending balance
 *
 * @param null $user_id
 *
 * @return float|int
 */
function referral__get_pending_balance($user_id = null) {
    global $wpdb;

    if( $user_id == null ) {
	    $user_id = get_current_user_id();
    }

    $referral_user = referral__get_user();

    $pending_leads_count = $wpdb->get_var( "SELECT COUNT(*) FROM " . REFERRAL__TABLE_LEADS . " WHERE status = '" . REFERRAL__LEAD_PENDING . "' AND user_id = '{$user_id}'" );
    $pending_balance = $pending_leads_count * $referral_user->commission;

    return $pending_balance;
}

/**
 * Get referral total invitation sent
 *
 * @return null|string
 */
function referral__get_referral_total_invitation_sent() {
    global $wpdb;

    $total_invitations_sent = $wpdb->get_var( "SELECT COUNT(*) FROM " . REFERRAL__TABLE_LEADS . " WHERE user_id = '" . get_current_user_id() . "'" );
    return $total_invitations_sent;
}

function referral__is_user_withdraw_request() {
    global $wpdb;

	$requests = $wpdb->get_results( 'SELECT * FROM ' . REFERRAL__TABLE_WITHDRAW . ' WHERE user_id = "' . get_current_user_id() . '" AND status = "' . REFERRAL__WITHDRAW_IN_PROCESS . '"' );
	return !empty($requests);
}

/**
 * Get referral withdraw requests
 * @param int $offset
 *
 * @return array|null|object
 */
function referral__get_withdraw_requests($offset = 0) {
	global $wpdb;

	$requests = $wpdb->get_results( "
	SELECT withdraw.*, users.user_email, users.display_name, ref_users.blocked
    FROM " . REFERRAL__TABLE_WITHDRAW . " withdraw
    LEFT JOIN " . $wpdb->prefix . "users users ON users.id = withdraw.user_id
    LEFT JOIN " . REFERRAL__TABLE_USERS . " ref_users ON ref_users.user_id = withdraw.user_id
    ORDER BY ID DESC
    LIMIT 20 OFFSET {$offset}" );

	return $requests;
}

/**
 * Get referral withdraw requests
 *
 * @return int
 */
function referral__get_withdraw_requests_count() {
	global $wpdb;

	$count = $wpdb->get_var( "SELECT COUNT(*) FROM " . REFERRAL__TABLE_WITHDRAW );

	return $count;
}

/**
 * Add referral withdraw request
 *
 * @param $amount
 * @param $type
 * @param $data
 */
function referral__add_withdraw_request($amount, $type, $data) {
    global $wpdb;

    $user_id = get_current_user_id();

    $wpdb->insert(
        REFERRAL__TABLE_WITHDRAW,
        array(
            'user_id' => $user_id,
            'amount' => $amount,
            'type' => $type,
            'data' => $data,
            'date' => time(),
            'status' => REFERRAL__WITHDRAW_IN_PROCESS
        )
    );

    # remove money amount from balance
	referral__update_user_amount($amount, FALSE, $user_id);
}

/**
 * Update referral withdraw request
 *
 * @param $id
 * @param $status
 * @param $withdraw_request
 */
function referral__update_withdraw_request($id, $status, $withdraw_request) {
    global $wpdb;

    $wpdb->update(
        REFERRAL__TABLE_WITHDRAW,
        array(
            'status' => $status
        ),
        array(
            'ID' => $id
        )
    );

    switch ( $status ) {
        case REFERRAL__WITHDRAW_PAID: {
	        $user_obj = referral__get_user($withdraw_request->user_id);

	        $new_earning = $user_obj->earning + $withdraw_request->amount;

	        $wpdb->update(
		        REFERRAL__TABLE_USERS,
		        array(
			        'earning' => $new_earning
		        ),
		        array(
			        'user_id' => $withdraw_request->user_id
		        )
	        );
        } break;
    }
}

/**
 * Get Withdraw Request
 * @param $id
 *
 * @return object
 */
function referral__get_withdraw_request($id) {
    global $wpdb;

	$withdraw_request = $wpdb->get_row( "SELECT * FROM " . REFERRAL__TABLE_WITHDRAW . " withdraw WHERE ID = {$id}" );

	return $withdraw_request;
}

/**
 * Update user field
 *
 * @param $user_id
 * @param $name
 * @param $value
 */
function referral__update_user_fields($user_id, $name, $value) {
	global $wpdb;

	$wpdb->update(
		REFERRAL__TABLE_USERS,
		array(
			$name => $value
		),
		array(
			'user_id' => $user_id
		)
	);
}

/**
 * Get referrals
 *
 * @param int $offset
 * @param string $order_by
 * @param string $order
 * @param string $search_val
 *
 * @return array|null|object
 */
function referral__get_users($offset = 0, $order_by = 'ID', $order = 'DESC', $search_val = '') {
    global $wpdb;

    $search_query = '';
    if( $search_val != '' ) {
        $search_query = "WHERE users.user_email LIKE '%{$search_val}%' OR users.display_name LIKE '%{$search_val}%'";
    }

    $query = "
        SELECT ref__users.*, users.user_email, users.display_name, withdraw.amount AS 'pending'
        FROM " . REFERRAL__TABLE_USERS . " ref__users
        LEFT JOIN " . $wpdb->prefix . "users users ON users.id = ref__users.user_id
        LEFT JOIN " . REFERRAL__TABLE_WITHDRAW . " withdraw ON withdraw.user_id = ref__users.user_id 
        AND (withdraw.status = ". REFERRAL__WITHDRAW_IN_PROCESS . " OR withdraw.status = " . REFERRAL__WITHDRAW_SENT . ")
        {$search_query}
        ORDER BY {$order_by} {$order} 
        LIMIT 20 
        OFFSET {$offset}";

	$users = $wpdb->get_results($query);

    return $users;
}

/**
 * Get referral users count
 *
 * @param $search_val
 *
 * @return string|null
 */
function referral__get_users_count($search_val) {
	global $wpdb;

    $search_query = '';
    if( $search_val != '' ) {
        $search_query = "WHERE users.user_email LIKE '%{$search_val}%' OR users.display_name LIKE '%{$search_val}%'";
    }

    $query = "
        SELECT COUNT(*)
        FROM " . REFERRAL__TABLE_USERS . " ref__users
        LEFT JOIN " . $wpdb->prefix . "users users ON users.id = ref__users.user_id
        {$search_query}";

	$count = $wpdb->get_var( $query );

	return $count;
}

/**
 * Block/Unlock referral user
 *
 * @param $user_id
 * @param $type
 */
function referral__block_unlock_user($user_id, $type) {
	global $wpdb;

	$wpdb->update(
		REFERRAL__TABLE_USERS,
		array(
			'blocked' => $type
		),
		array(
			'user_id' => $user_id
		)
	);
}

/**
 * Get lead information
 *
 * @param $invite_hash
 * @return array|object|bool
 */
function referral__get_lead_object($invite_hash) {
    global $wpdb;

    $lead = $wpdb->get_results( "
        SELECT ref__leads.*, users.display_name
        FROM " . REFERRAL__TABLE_LEADS . " ref__leads
        LEFT JOIN " . $wpdb->prefix . "users users ON users.id = ref__leads.user_id
        WHERE invite_hash = '{$invite_hash}'"
    );

    if( count( $lead ) > 0 ) {
        return $lead[0];
    }
    else {
        return FALSE;
    }
}
