<pre>
<?php

/**
 * Check leads with status SENT and change status to PENDING or EXPIRED if needed
 */

require_once( dirname( dirname( dirname( dirname( dirname( __FILE__ ))))) . '/wp-load.php' );

global $wpdb;

# get leads with status SENT
$leads = $wpdb->get_results( "SELECT * FROM " . REFERRAL__TABLE_LEADS . " WHERE status = '" . REFERRAL__LEAD_SENT . "'" );

$pending_leads = 0;
$expired_leads = 0;
foreach ( $leads as $lead ) {
	# check is lead became a user
	$lead_user = get_user_by( 'email', $lead->email );

	if ( ! empty( $lead_user ) ) {
		$pending_leads ++;

		$wpdb->update(
			REFERRAL__TABLE_LEADS,
			array(
				'status'       => REFERRAL__LEAD_PENDING,
				'pending_date' => time()
			),
			array(
				'ID' => $lead->ID
			)
		);

		# give referral user one more invitation
		$referral_user = $wpdb->get_row( "SELECT * FROM " . REFERRAL__TABLE_USERS . " WHERE user_id = '{$lead->user_id}'" );
		$wpdb->update(
			REFERRAL__TABLE_USERS,
			array(
				'invitations_used' => $referral_user->invitations_used - 1
			),
			array(
				'user_id' => $referral_user->user_id
			)
		);

	} # if lead do not register as user, check is lead expired
	else {
		$expired_date = $lead->referred_date + ( 60 * 60 * 24 * REFERRAL__TO_PENDING_DAYS );
		# if current time more then expire time, this lead moving to inactive with status EXPIRED
		if ( time() > $expired_date ) {
			$expired_leads ++;

			$wpdb->update(
				REFERRAL__TABLE_LEADS,
				array(
					'status' => REFERRAL__LEAD_EXPIRED
				),
				array(
					'ID' => $lead->ID
				)
			);

			# give referral user one more invitation
			$referral_user = $wpdb->get_row( "SELECT * FROM " . REFERRAL__TABLE_USERS . " WHERE user_id = '{$lead->user_id}'" );
			$wpdb->update(
				REFERRAL__TABLE_USERS,
				array(
					'invitations_used' => $referral_user->invitations_used - 1
				),
				array(
					'user_id' => $referral_user->user_id
				)
			);
		}
	}
}
echo "changed leads status from SENT to PENDING - " . $pending_leads . "<br>";
echo "changed leads status from SENT to EXPIRED - " . $expired_leads . "<br>";
echo "leads check - " . count( $leads );
?>
</pre>
