<pre>
<?php

/**
 * Check leads with status PENDING and change status to ELIGIBLE if needed
 */

require_once( dirname( dirname( dirname( dirname( dirname( __FILE__ ))))) . '/wp-load.php' );

global $wpdb;

# get leads with status PENDING
$leads = $wpdb->get_results( "SELECT * FROM " . REFERRAL__TABLE_LEADS . " WHERE status = '" . REFERRAL__LEAD_PENDING . "'" );

$eligible_leads = 0;
foreach ( $leads as $lead ) {
	$eligible_date = $lead->pending_date + ( 60 * 60 * 24 * REFERRAL__PENDING_DAYS );
	if( $eligible_date < time() ) {
		$eligible_leads ++;

	    # change lead status to ELIGIBLE
		$wpdb->update(
			REFERRAL__TABLE_LEADS,
			array(
				'status' => REFERRAL__LEAD_ELIGIBLE
			),
			array(
				'ID' => $lead->ID
			)
		);

		# add money to referral
		$referral_user = $wpdb->get_row( "SELECT * FROM " . REFERRAL__TABLE_USERS . " WHERE user_id = '{$lead->user_id}'" );

		$wpdb->update(
			REFERRAL__TABLE_USERS,
			array(
				'balance' => $referral_user->balance + $referral_user->commission
			),
			array(
				'user_id' => $referral_user->user_id
			)
		);
	}
}

echo "changed leads status from PENDING to ELIGIBLE - " . $eligible_leads . "<br>";
echo "leads check - " . count( $leads );
?>
</pre>
