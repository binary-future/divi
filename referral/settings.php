<?php

define( 'REF__HOME_DIR', get_stylesheet_directory_uri() );

# Referral tables
define( 'REFERRAL__TABLE_LEADS', 'referral__leads' );
define( 'REFERRAL__TABLE_SETTINGS', 'referral__settings' );
define( 'REFERRAL__TABLE_USERS', 'referral__users' );
define( 'REFERRAL__TABLE_WITHDRAW', 'referral__withdraw_requests' );

# Referral Leads Status
define( 'REFERRAL__LEAD_EXPIRED', 1 ); // link expired
define( 'REFERRAL__LEAD_SENT', 2 ); // link was sent to friend
define( 'REFERRAL__LEAD_PENDING', 3 ); // friend from this link was registered less then 45 days ago
define( 'REFERRAL__LEAD_ELIGIBLE', 4 ); // friend from this link was registered more then 45 days ago, so money for this LEAD was added to user balance

# Referral Withdraw Requests Status
define( 'REFERRAL__WITHDRAW_IN_PROCESS', 1 );
define( 'REFERRAL__WITHDRAW_PAID', 2 );
define( 'REFERRAL__WITHDRAW_VOID', 3 );
define( 'REFERRAL__WITHDRAW_SENT', 4 );

# Other Settings
define( 'REFERRAL__PENDING_DAYS', 45 ); // number of days from PENDING lead status to ELIGIBLE
define( 'REFERRAL__TO_PENDING_DAYS', 14 ); // number of days when lead can create account and give money to user