<?php
/**
 * Template Name: Referral Invitation
 */

if( isset( $_GET['c']) ) {

    $lead_obj = referral__get_lead_object( $_GET['c'] );

    if( $lead_obj ) {
        $expired_date = $lead_obj->referred_date + 60*60*24*REFERRAL__TO_PENDING_DAYS;
        $expired_date_human = date('m-d-Y',$expired_date);

        $expired_link = 'https://www.dropshiplifestyle.com/friend-invitation-expired/?user=' . $lead_obj->display_name . '&first_name=' . $lead_obj->name;
        $not_expired_link = 'https://www.dropshiplifestyle.com/friend-invitation/?user=' . $lead_obj->display_name . '&first_name=' . $lead_obj->name . '&date=' . $expired_date_human;

        // check is lead link is not expired
        if( $expired_date > time() ) {
            // get user who send invite
            $referral_user = referral__get_user();

            // check is user have available invitations
            $invitations_left = $referral_user->invitations_total - $referral_user->invitations_used;

            // check is invitations left
            if( $invitations_left > 0 ) {
                wp_redirect( $not_expired_link );
            }
            else {
                wp_redirect( $expired_link );
            }
        }
        else {
            wp_redirect( $expired_link );
        }
    }
    else {
        ?>
        <h1>Wrong Invitation Code...</h1>
        <?php
    }
}
else {
    ?>
    <h1>Wrong Invitation Code...</h1>
    <?php
}